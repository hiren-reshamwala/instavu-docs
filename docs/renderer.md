To use the renderer API you must include javascript library into your html file.

## Initialize Viewport

    var viewport = new InstaVU(hostUrl, apikey, containerElement, width, height); 

### Parameters

-   `hostUrl` **[String][1]?** Server URL (Required) - `https://instavu3d.com/{instanceName}/`
-   `apikey` **[String][1]?** API Key required to access the data from server (Required)
-   `containerElement` **[HTMLElement][7]?** Where you want to append the 3D canvas element
-   `width` **[Number][8]** canvas element width (optional, default : Container element width)
-   `height` **[Number][8]** canvas element height (optional, default : Container element height)

#### Returns

`viewport` **[Object][5]?** ViewportObject

Example:

    var viewport = new InstaVU('https://www.instavu3d.com/{instanceName}',{APIKEY}, document.getElementById('threed_container'));


### Functions

## resize

Resize the viewport

#### Parameters

-   `width` **[Number][8]?** (Required)
-   `height` **[Number][8]?** (Required)

Example:

    viewport.resize(width,height);

## load

Load 3d scene into the viewport _(You will get callback on loadCompleted function after completion of load scene)_

#### Parameters

-   `projectid` **[String][1]?** Reference id of scene (Required) - _find in projects data API_

Example:

    viewport.load('{SceneReferenceId}');

#### Returns
**-** 

## loadCompleted

This is callback function which will call after complete load new scene

#### Parameters
**-** 

#### Returns

`surfaces` **[Array][9]?** Array of objects

-   `name` **[String][1]**
-   `image` **[String][1]** `https://www.instavu3d.com/{instanceName}/projects/assets/{projectid}/{image}`

Example:
    
    viewport.loadCompleted = function(surfaces){

    };

Sample return:

    [{
		'name' : 'surface1',
		'image' : 'surface1_thumb.jpg'
	},{
		'name' : 'surface2',
        'image' : 'surface2_thumb.jpg'
	}]

## removeSurfaceByName

This function will remove the surface from viewport if it is on viewport

#### Parameters

-   `name` **[String][1]?** Name of the surface (Required)

#### Returns
**-** 

Example:
    
    viewport.removeSurfaceByName('{surfaceName}');


## loadSurfaceByName

This function will load the surface if it is available

#### Parameters

-   `name` **[String][1]?** Name of the surface (Required)

#### Returns
**-** 

Example:
    
    viewport.loadSurfaceByName('{surfaceName}');



## getSelectedSurface

This function will give you the name of current selected surface

#### Parameters
**-** 

#### Returns

`name` **[String][1]?** Name of the surface

Example:

    var currentSurfaceName = viewport.getSelectedSurface();


## resetPosition

Reset the position of the scene loaded on viewport

#### Parameters
**-** 

#### Returns
**-** 

Example:

    viewport.resetPosition();


## loadTexture

Render the texture on the surface of the scene _(Find the texture in designs data API)_

#### Parameters

-   `name` **[String][1]?** Surface name
-   `imageUrl` **[String][1]?** Design image url `https://instavu3d.com/{instanceName}/texture/{referenceId}`
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onProgress` **[Function][2]?** called on progress with one argument `(progress)`.
-   `onError` **[Function][2]?** called on fail to load with one argument `(error)`. **[Error][3]?**

#### Returns
**-** 

Example:

    var url = 'https://instavu3d.com/{instanceName}/texture/{referenceId}';
    viewport.loadTexture(url,'spread_collar',options,function(){
        //Call on success   
    }, function(progress){
        //Call on progress
    }, function(error){
        //Call on error
    });


## loadTexture2

Render the texture on the surface of the scene _(Find the texture in designs data API)_

#### Parameters

-   `name` **[String][1]?** Surface name
-   `imageUrl` **[String][1]?** Design image url `https://instavu3d.com/{instanceName}/texture/{referenceId}`
-   `options` **[Object][5]?** (required)
    -   `options.shininess` **[Number][8]** Increase/Decrease shine - float 1 to 1000 (optional, default : 1)
    -   `options.isRepeat` **[Boolean][6]** Set true if you want to repeat the texture (optional, default : false)
    -   `options.autoRepeat` **[Boolean][6]** Set true if you want to set repeat value as per 3d object (optional, default : true) _This will use of isRepeat option is true_
    -   `options.repeatX` **[Number][8]** Number of repeat texture in x axis - float 0 to 1000 (optional, default : -1) _This will use if isRepeat option is true and autoRepeat option is false_
    -   `options.repeatY` **[Number][8]** Number of repeat texture in y axis - float 0 to 1000 (optional, default : -1) _This will use if isRepeat option is true and autoRepeat option is false_
    -   `options.transparency` **[Number][8]** change the transparency of 3d object - float 0 to 1 (optional, default : 1)
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onProgress` **[Function][2]?** called on progress with one argument `(progress)`.
-   `onError` **[Function][2]?** called on fail to load with one argument `(error)`. **[Error][3]?**

#### Returns
**-** 

Example:

	var url = 'https://instavu3d.com/{instanceName}/texture/{referenceId}';
	var options = {
		isRepeat: true,
        autoRepeat: true
	};
    viewport.loadTexture2('{surfaceName}',url,options,function(){
    	//Call on success	
	}, function(progress){
		//Call on progress
	}, function(error){
        //Call on error
    });


## loadColor

Render the RGB color on the surface

#### Parameters

-   `name` **[String][1]?** Surface name
-   `r` **[Number][8]?** Red color of RGB - Integer 0 to 255 (required)
-   `g` **[Number][8]?** Green color of RGB - Integer 0 to 255 (required)
-   `b` **[Number][8]?** Blue color of RGB - Integer 0 to 255 (required)

#### Returns
**-** 

Example:

	var url = 'https://instavu3d.com/{instanceName}/texture/{referenceId}';
	var options = {};
    viewport.loadColor('{surfaceName}',212,112,32);

## toImage

You can get base64 image of the viewport

#### Parameters
**-** 

#### Returns

`image` **[String][1]?** Base64 Image of the viewport

Example: 

    var imageURL = viewport.toImage();

## getImageBlob

You can get Blob type image of the viewport

#### Parameters
**-** 

#### Returns

`image` **[Blob][10]?** Image of the viewport

Example: 

    var imageBlob = viewport.getImageBlob();



[1]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[2]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function

[3]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Error

[4]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise

[5]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[6]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean

[7]: https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement

[8]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[9]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

[10]: https://developer.mozilla.org/en-US/docs/Web/API/Blob