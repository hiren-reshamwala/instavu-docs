In order to use the data API you need an API key. This API key can be found on the account page if you have the right to use API or contact administrator.

You have to use the API key in querystring(?apiKey=1234).

    https://instavu3d.com/{instanceName}/api/v1/{documentType}

**Document Types**

* `scenes`
* `designs`

## designs

### - GET /stream/{Offset}/{Limit}

Returns a list of entities

The default values are:

    Offset = 0
    Limit = 30

You can use following query parameters with stream

* search (string)
:  `/api/v1/designs/stream?search={searchTerms}`
* labelids (Multiple numeric ids for the same name need to be splitted by ,)
:  `/api/v1/designs/stream?labelids=1,2,3`
* order (orderType "a"(ascending order) or "d"(descending order), default value is "d")
:  `/api/v1/designs/stream?order={orderType}`

##### Request (http)

    https://instavu3d.com/{instanceName}/api/v1/designs/stream/0/30?apikey=xxxx

##### Response (json)

    {
	    "status": true,
	    "data": {
	        "values": [{
	            "Id": 1,
	            "Name": "design-1",
	            "Description": null,
	            "FileName": "design-1.jpg",
	            "FileWidth": 1115,
	            "FileHeight": 1080,
	            "FileSize": "494.51",
	            "ReferenceId": "design-1.jpg",
	            "Url": "http://instavu3d.com/{instanceName}/api/v1/designs/image/design-1.jpg?width=300"
	        }, {
	            "Id": 423,
	            "Name": "design-2",
	            "Description": null,
	            "FileName": "design-2.jpg",
	            "FileWidth": 1080,
	            "FileHeight": 1080,
	            "FileSize": "656.53",
	            "ReferenceId": "design-2.jpg",
	            "Url": "http://instavu3d.com/{instanceName}/api/v1/designs/image/design-2.jpg?width=300"
	        }],
	        "count": 2
	    }
	}

### - GET Image /designs/image/{referenceId}

Parameters

* width (integer) - optional
* height (integer) - optional
    
Example: 

    https://instavu3d.com/{instanceName}/api/v1/designs/image/{referenceId}?width={number}&height={number}


### - GET /labels/designs

Get a list of labels for document type designs

##### Request (http)
    https://instavu3d.com/{instanceName}/api/v1/labels/designs/?apikey=xxxx

##### Response (json)
    {
	    "status": true,
	    "data": [{
	        "Id": 1,
	        "Name": "Tailormate designs",
	        "Parent": 0
	    }, {
	        "Id": 2,
	        "Name": "type1",
	        "Parent": 1
	    }, {
	        "Id": 3,
	        "Name": "XYZ Designs",
	        "Parent": 0
	    }]
	}

### - GET /labels/designs/{parentId}

Returns a list of child labels of particular parent.

##### Request (http)
    https://instavu3d.com/{instanceName}/api/v1/labels/designs/1/?apikey=xxxx

##### Response (json)
    {
	    "status": true,
	    "data": [{
	        "Id": 2,
	        "Name": "type1",
	        "Parent": 1
	    }]
	}


## projects

You will get the list of scenes which will load on viewport

### - GET /stream/{Offset}/{Limit}

Returns a list of entities

The default values are:

    Offset = 0
    Limit = 30

You can use following query parameters with stream

* search (string)
:  `/api/v1/projects/stream?search={searchTerms}`
* labelids (Multiple numeric ids for the same name need to be splitted by ,)
:  `/api/v1/projects/stream?labelids=1,2,3`
* order (orderType "a"(ascending order) or "d"(descending order), default value is "d")
:  `/api/v1/projects/stream?order={orderType}`

##### Request (http)

    https://instavu3d.com/{instanceName}/api/v1/projects/stream/0/30?apikey=xxxx

##### Response (json)

    {
	    "status": true,
	    "data": {
	        "values": [{
	            "Id": 1,
	            "Name": "Laptop",
	            "ImageName": "xyzabcd.png",
	            "ReferenceId": "ad3a399e3bbf719702bd7c6d83463800",
	            "Url": "http://instavu3d.com/{instanceName}/thumbnail/{ReferenceId}",
	        }],
	        "count": 1
	    }
	}